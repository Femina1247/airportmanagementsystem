package dao;

import java.util.List;

import model.Hangars;
import model.Planes;


public interface IHangarDao {
	public void insertHangars(Hangars hangars);
	public List<Hangars> view();
	public void updateHangars(Hangars hangars);

}
