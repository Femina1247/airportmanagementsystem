package dao;

import java.util.List;

import model.Pilots;
import model.Planes;


public interface IPilotDao {
	public void insertPilots(Pilots pilots);
	public List<Pilots> view();
	public void updatePilots(Pilots pilots);

}
